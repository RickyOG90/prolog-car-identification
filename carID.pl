/*
Car Identification game

Riccardo Gutierrez U70795314
Saurabh Hinduja U34041272

Description: Program's job is dentifying car types. View README to understand the selected car types

Start with ?- start.*/


start :- guess(Vehicle),
	write('I believe the car type is: '),
	write(Vehicle),
	nl,
	undo.
	
/*Guesses to test*/
guess('antique car') :- antique, !.
guess('pickup truck') :- pickup, !.
guess('super car') :- super, !.
guess('sports car') :- sports, !.
guess('muscle car') :- muscle, !.
guess('classic muscle car') :- classic_muscle, !.
guess('classic sports car') :- classic_sports, !.
guess('luxury car') :- luxury, !.
guess('exotic car') :- exotic, !.
guess('basic car'). /* no diagnosis*/


/* car identification rules */
antique :- verify('pre 1930?').

sports :- modern,
		  sporty,
		  expensive,
		  lux,
		  performance,
		  nimble.

super :- modern,
		 nimble,
		 lux,
		 vexpensive,
		 tech,
		 performance.
		 

muscle :- modern,
		  performance,
		  sporty,
		  heavy,
		  verify('currently affordable? ').

classic_muscle :- classic,
				 performance,
				 rare,
				 heavy,
				 twodoor,
				 verify('inexpensive when it was new? ').

classic_sports :- classic,
				 nimble,
				 lux,
				 performance,
				 sporty,
				 rare.

luxury :- modern,
		  tech,
		  expensive,
		  performance,
		  heavy,
		  lux.
			 

exotic :- lux,
		  tech,
		  twodoor,
		  expensive,
		  performance,
		  rare.
		  
pickup :- truck.
			 

/*classification rules */
classic :- verify('older than 25 years? ').

modern :- verify('within 25 years? ').

lux :- verify('has luxury options? '), !.
lux :- fourdoor.
	   
vexpensive :- verify('exceedingly expensive? (>$300k)').

expensive :- verify('kind of expensive? (>$60k)').

sporty :- verify('is convertible? '), !.
sporty :- verify('is two seater? '), !.
sporty :- twodoor, !.
sporty :- fourdoor.

performance :- verify('high top speed? (>150mph)'), !.
performance :- verify('high acceleration? (0-60mph >5 sec)'), !.
performance :- verify('high HP and Torque? ').

nimble :- verify('high manueverability? ').

rare :- verify('few currently exists? (<20k)'), !.
rare :- verify('low production? (<100k)').

twodoor :- verify('has two doors? ').

fourdoor :- verify('has four doors? ').

heavy :- verify('weighs alot? (>3000lbs)').

tech :- verify('technologically advanced? ').

truck :- lux, performance, heavy, bed.

bed :- verify('does the vehicle have a bed?').
	
/* Questions */
ask(Question) :-
	write('Does the car have the following attribute: '),
	write(Question),
	nl,
	read(Response),
	nl,
	( (Response == yes ; Response == y)
		->
		asserta(yes(Question)) ;
		asserta(no(Question)), fail).
		
:- dynamic(yes/1,no/1).


/* Verification */
verify(S) :-
	(yes(S)
	->
	true ;
	(no(S)
	->
	fail ;
	ask(S))).

	
/* at the end, undo all yes andor no assertions */
	undo :-retract(yes(_)), fail.
	undo :-retract(no(_)), fail.
	undo.